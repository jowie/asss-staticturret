#ifndef __STATICTURRET_H
#define __STATICTURRET_H

#include <stdbool.h>

#define I_STATICTURRET "staticturret-2"
/* pyinclude: staticturret/staticturret.h */

enum ADDBOT_ERROR
{
	ADDBOT_OK = 0,
	ADDBOT_ILLEGAL_ARENA, // module has not been attached in this arena
	ADDBOT_UNKNOWN_TYPE,
	ADDBOT_MAX_REACHED_FOR_BOTTYPE,
	ADDBOT_MAX_REACHED_FOR_ARENA,
	ADDBOT_CAN_NOT_BE_PLACED_ON_MAP, // bot is being placed on a tiled section of the map
	ADDBOT_TO_CLOSE_TO_OTHER_BOT, // only if noLocationCheck = false
	ADDBOT_BUILDING_IN_PROGRESS, // build sequence is enabled for the bot and to many other bots are currently being constructed
};

typedef struct Istaticturret
{
	INTERFACE_HEAD_DECL
	/* pyint: use */

	void (*StartGame)(Arena *arena);
	/* pyint: arena_not_none -> void */

	void (*StopGame)(Arena *arena);
	/* pyint: arena_not_none -> void */

	// x and y are in pixels. (to convert tiles to pixels, use: (tilex << 4) + 8
	enum ADDBOT_ERROR (*AddBot)(Arena *arena, const char *key, int x, int y, int freq, bool infiniteRespawn, bool noLocationCheck);
	/* pyint: arena_not_none, string, int, int, int, int, int -> int */

	void (*FreezeRespawn)(Arena *arena, int freq, bool freeze);
	/* pyint: arena_not_none, int, int -> void */

	//Set the amount of power that a freq has. Note: this has to be called at least once to work
	void (*SetPower)(Arena *arena, int freq, int power);
	/* pyint: arena_not_none, int, int -> void */

	void (*DoDamage)(Arena *arena, Player *killer, int x, int y, int damage, int radius, int immuneFreq);
	/* pyint: arena_not_none, player_not_none, int, int, int, int, int -> void */

} Istaticturret;

#endif

